package vistas;

import java.awt.Color;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import modelo.dao.CompraController;
import modelo.dao.CompraControllerImpl;
import modelo.dao.InventarioController;
import modelo.dao.InventarioControllerImpl;
import modelo.dao.ProveedorController;
import modelo.dao.ProveedorControllerImpl;
import modelo.entidades.Compra;
import modelo.entidades.CompraDet;
import modelo.entidades.Inventario;
import modelo.entidades.Proveedor;
import modelo.excepciones.CompraException;
import modelo.excepciones.InventarioException;
import modelo.excepciones.ProveedorException;
import utilidades.CompraTableModel;

public class vistaCompra extends javax.swing.JFrame {

    public vistaCompra() {
        initComponents();
        this.setLocationRelativeTo(null);
        PanelEditar.setVisible(false);
        PanelConsultar.setVisible(false);
        
    }

    @SuppressWarnings("unchecked")
                           
  private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        PanelConsultar = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblconsulta = new javax.swing.JTable();
        PanelEditar = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        btncomprar = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        nombreEmpresa2 = new javax.swing.JLabel();
        cmbproveedor = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblcompra = new javax.swing.JTable();
        logoempresa = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnconsultar = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btnagregar = new javax.swing.JLabel();
        btneliminarfla = new javax.swing.JButton();
        agregarfila = new javax.swing.JButton();

        setTitle("EASY START - COMPRA");
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 125, 99));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Open Sans", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 102));
        jLabel3.setText("COMPRA");
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo1_1.jpg"))); // NOI18N
        jPanel4.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, -30, -1, -1));

        PanelConsultar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblconsulta.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        
        jScrollPane1.setViewportView(tblconsulta);

        PanelConsultar.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 80, 530, 240));

        jPanel4.add(PanelConsultar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, 550, 430));

        PanelEditar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel9.setBackground(new java.awt.Color(0, 125, 99));
        jLabel9.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 51, 51));
        jLabel9.setText("PROVEEDOR");
        PanelEditar.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, -1));

        btncomprar.setBackground(new java.awt.Color(0, 204, 204));
        btncomprar.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btncomprar.setForeground(new java.awt.Color(0, 51, 51));
        btncomprar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btncomprar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-más-32.png"))); // NOI18N
        btncomprar.setText("GUARDAR");
        btncomprar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btncomprar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btncomprar.setIconTextGap(20);
        
        btncomprar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncomprarMouseClicked(evt);
            }
        });
        PanelEditar.add(btncomprar, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 370, 160, 50));

        jPanel9.setBackground(new java.awt.Color(250, 175, 4));
        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        nombreEmpresa2.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        nombreEmpresa2.setForeground(new java.awt.Color(255, 255, 255));
        jPanel9.add(nombreEmpresa2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 250, 40));

        PanelEditar.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 370, 160, 50));

        cmbproveedor.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        
        PanelEditar.add(cmbproveedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 20, 590, 40));

        
        jScrollPane2.setViewportView(tblcompra);

        PanelEditar.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 730, 200));
        
        btneliminarfla.setText("ELIMINAR");
        PanelEditar.add(btneliminarfla, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 290, -1, -1));

        agregarfila.setText("AGREGAR");
        PanelEditar.add(agregarfila, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 290, -1, -1));
        
        /*TableCellEditor editorCelda = tblcompra.getDefaultEditor(Object.class);
        
        ((DefaultCellEditor)editorCelda).setClickCountToStart(1);
        
        JComboBox< Inventario > jc = new JComboBox<>();  
        InventarioController inv = new InventarioControllerImpl();
        List<Inventario> inventario;
        try {
            inventario = inv.list();
             for(Inventario i : inventario){
        jc.addItem(i);
        }
        
        // Crear editor de celda asociado al combo
        TableCellEditor comboEditor = new DefaultCellEditor(jc);
        
        // Asociar editor en la tabla para columna de tipo Alumno
        tblcompra.setDefaultEditor(CompraDet.class, comboEditor);
        
        // Asociar generador de contenido (render) en la tabla para columna de 
        // tipo Alumno
        tblcompra.setDefaultRenderer(CompraDet.class, new LabelRenderer());
        } catch (InventarioException ex) {
            Logger.getLogger(vistaCompra.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
        try {
        CompraTableModel modelo = new CompraTableModel();
        tblcompra.setModel(modelo);
        tblcompra.getTableHeader().setReorderingAllowed(false);
        tblcompra.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tblcompra.setAutoCreateRowSorter(true);
        
        
        InventarioController inv = new InventarioControllerImpl();
        List<Inventario> inventario;
        
            inventario = inv.list();
        
        for(Inventario i : inventario){
        jc.addItem(i);
        }
        TableColumn columna = tblcompra.getColumnModel().getColumn(0);
        
        TableCellEditor editor = new DefaultCellEditor(jc);
        
        columna.setCellEditor(editor);
        } catch (InventarioException ex) {
            Logger.getLogger(vistaCompra.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        CompraTableModel modelo = new CompraTableModel();
        agregarfila.addActionListener(ae -> {
            modelo.addRow(new CompraDet());
        });

        
        btneliminarfla.addActionListener(ae -> {
            // Recuperar indice fila seleccionada en la lista
            int rowIndex = tblcompra.getSelectedRow();

            if (rowIndex >= 0) {
                modelo.deleteRow(rowIndex);
            }

        });

        jPanel4.add(PanelEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 750, 440));

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 0, 936, 590));
        jPanel1.add(logoempresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 24, 240, 100));

        jLabel2.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Empresa:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, -1, -1));

        btnconsultar.setBackground(new java.awt.Color(255, 255, 255));
        btnconsultar.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btnconsultar.setForeground(new java.awt.Color(0, 51, 51));
        btnconsultar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnconsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-archivo-de-verificación-32 (1).png"))); // NOI18N
        btnconsultar.setText("CONSULTAR");
        btnconsultar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnconsultar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnconsultar.setIconTextGap(20);
        btnconsultar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnconsultarMouseClicked(evt);
            }
        });
        jPanel1.add(btnconsultar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 290, 50));

        jPanel3.setBackground(new java.awt.Color(250, 175, 4));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 290, 50));

        jPanel5.setBackground(new java.awt.Color(250, 175, 4));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnagregar.setBackground(new java.awt.Color(0, 153, 153));
        btnagregar.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btnagregar.setForeground(new java.awt.Color(0, 51, 51));
        btnagregar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-más-32.png"))); // NOI18N
        btnagregar.setText("AGREGAR");
        btnagregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnagregar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnagregar.setIconTextGap(20);
        btnagregar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnagregarMouseClicked(evt);
            }
        });
        jPanel5.add(btnagregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 290, 50));

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 290, 290, 50));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1093, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 579, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                        

    private void btnconsultarMouseMoved(java.awt.event.MouseEvent evt) {                                        
        btnconsultar.setBorder(BorderFactory.createLineBorder(new Color(153,153,153)));
    }                                       

    private void btnconsultarMouseExited(java.awt.event.MouseEvent evt) {                                         
      btnconsultar.setBorder(BorderFactory.createLineBorder(new Color(0,125,99)));
    }                                                                                                                    

    private void btncomprarMouseClicked(java.awt.event.MouseEvent evt) {                                        
         try {
        Integer idproveedor = cmbproveedor.getItemAt(cmbproveedor.getSelectedIndex()).getIdproveedor();
        Compra  c = new Compra(idproveedor);
        CompraController comp = new CompraControllerImpl();
        Compra crear = comp.crear(c);
        } catch (CompraException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error Aplicación",
                        JOptionPane.ERROR_MESSAGE);
            }
        JOptionPane.showMessageDialog(null, "Se ha eliminado el proveedor");
    }                                                                        

    private void btnagregarMouseExited(java.awt.event.MouseEvent evt) {                                       
        btnagregar.setBorder(BorderFactory.createLineBorder(new Color(0,125,99)));
    }                                      

    private void btnagregarMouseMoved(java.awt.event.MouseEvent evt) {                                      
        btnagregar.setBorder(BorderFactory.createLineBorder(new Color(153,153,153)));
    }     
    private void btnagregarMouseClicked(java.awt.event.MouseEvent evt) {                                        
        
        PanelEditar.setVisible(true);
        PanelConsultar.setVisible(false);
        
        try {
       
        ProveedorController p = new ProveedorControllerImpl();
        
        List<Proveedor> proveedores = p.list();

        for (Proveedor prov : proveedores) {
            
            cmbproveedor.addItem(prov); 
        }
        
        
        } catch (ProveedorException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error Aplicación",
                        JOptionPane.ERROR_MESSAGE);
        }
        
       

    }  
    private void btnconsultarMouseClicked(java.awt.event.MouseEvent evt) {                                        
        
        PanelEditar.setVisible(false);
        PanelConsultar.setVisible(true);
        
    }  
                  
    private javax.swing.JPanel PanelConsultar;
    private javax.swing.JPanel PanelEditar;
    private javax.swing.JLabel btnagregar;
    private javax.swing.JLabel btncomprar;
    private javax.swing.JLabel btnconsultar;
    private javax.swing.JComboBox<Proveedor> cmbproveedor;
   // private javax.swing.JComboBox<Inventario> cmbproducto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel logoempresa;
    private javax.swing.JLabel nombreEmpresa2;
    private javax.swing.JTable tblcompra;
    private javax.swing.JTable tblconsulta;
    private javax.swing.JButton btneliminarfla;
    private javax.swing.JButton agregarfila;
    JComboBox< Inventario > jc = new JComboBox<>();  
    // End of variables declaration                   
}
