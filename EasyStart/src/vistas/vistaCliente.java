package vistas;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class vistaCliente extends javax.swing.JFrame {

    public vistaCliente() {
        initComponents();
        this.setLocationRelativeTo(null);
        
    }

    @SuppressWarnings("unchecked")
                           
 private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        btnusuario = new javax.swing.JLabel();
        btnproveedor = new javax.swing.JLabel();
        btncliente = new javax.swing.JLabel();
        btncompra = new javax.swing.JLabel();
        btnproducto = new javax.swing.JLabel();
        btnbanco = new javax.swing.JLabel();
        btnventa = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        logoempresa = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        nombreEmpresa = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 125, 99));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnusuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnusuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-usuario-masculino-en-círculo-100.png"))); // NOI18N
        btnusuario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnusuario.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnusuario.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btnusuarioMouseMoved(evt);
            }
        });
        btnusuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnusuarioMouseExited(evt);
            }
        });
        jPanel4.add(btnusuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 330, 160, 170));

        btnproveedor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnproveedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-hombre-de-negocios-100.png"))); // NOI18N
        btnproveedor.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnproveedor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnproveedor.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btnproveedorMouseMoved(evt);
            }
        });
        btnproveedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnproveedorMouseExited(evt);
            }
        });
        jPanel4.add(btnproveedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 140, 150, 170));

        btncliente.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-hombre-de-negocios-filled-100.png"))); // NOI18N
        btncliente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btncliente.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btncliente.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btnclienteMouseMoved(evt);
            }
        });
        btncliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnclienteMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnclienteMouseExited(evt);
            }
        });
        jPanel4.add(btncliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 140, 150, 170));

        btncompra.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncompra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-agregar-a-carrito-de-compras-100.png"))); // NOI18N
        btncompra.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btncompra.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btncompra.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btncompraMouseMoved(evt);
            }
        });
        btncompra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncompraMouseExited(evt);
            }
        });
        jPanel4.add(btncompra, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 140, 150, 170));

        btnproducto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnproducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-producto-nuevo-100.png"))); // NOI18N
        btnproducto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnproducto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnproducto.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btnproductoMouseMoved(evt);
            }
        });
        btnproducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnproductoMouseExited(evt);
            }
        });
        jPanel4.add(btnproducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 330, 160, 170));

        btnbanco.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnbanco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-museo-100.png"))); // NOI18N
        btnbanco.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnbanco.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnbanco.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btnbancoMouseMoved(evt);
            }
        });
        btnbanco.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnbancoMouseExited(evt);
            }
        });
        jPanel4.add(btnbanco, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 330, 160, 170));

        btnventa.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnventa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-checkout-100.png"))); // NOI18N
        btnventa.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnventa.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnventa.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btnventaMouseMoved(evt);
            }
        });
        btnventa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnventaMouseExited(evt);
            }
        });
        jPanel4.add(btnventa, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 140, 150, 170));

        jLabel9.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(211, 84, 0));
        jLabel9.setText("USUARIOS");
        jLabel9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel4.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 470, -1, -1));

        jLabel10.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(211, 84, 0));
        jLabel10.setText("CLIENTE");
        jLabel10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel4.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 280, -1, -1));

        jLabel11.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(211, 84, 0));
        jLabel11.setText("PROVEEDOR");
        jLabel11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel4.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 280, -1, -1));

        jLabel12.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(211, 84, 0));
        jLabel12.setText("COMPRA");
        jLabel12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel4.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 280, -1, -1));

        jLabel13.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(211, 84, 0));
        jLabel13.setText("VENTA");
        jLabel13.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel4.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 280, -1, -1));

        jLabel14.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(211, 84, 0));
        jLabel14.setText("PRODUCTOS");
        jLabel14.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel4.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 470, -1, -1));

        jLabel15.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(211, 84, 0));
        jLabel15.setText("BANCOS");
        jLabel15.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel4.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 470, -1, -1));

        jLabel3.setFont(new java.awt.Font("Open Sans", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 102));
        jLabel3.setText("ADMINISTRACIÓN");
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo1_1.jpg"))); // NOI18N
        jPanel4.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, -30, -1, -1));

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 0, 936, 590));
        jPanel1.add(logoempresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 24, 240, 100));

        jLabel2.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Empresa:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, -1, -1));

        nombreEmpresa.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        nombreEmpresa.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(nombreEmpresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 250, 40));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1093, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 579, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                         

    private void btnclienteMouseMoved(java.awt.event.MouseEvent evt) {                                      
        btncliente.setBorder(BorderFactory.createLineBorder(new Color(153,153,153)));
    }                                     

    private void btnclienteMouseExited(java.awt.event.MouseEvent evt) {                                       
      btncliente.setBorder(BorderFactory.createLineBorder(new Color(255,255,255)));
    }                                      

    private void btnproveedorMouseMoved(java.awt.event.MouseEvent evt) {                                        
     btnproveedor.setBorder(BorderFactory.createLineBorder(new Color(211,84,0)));
    }                                       

    private void btnproveedorMouseExited(java.awt.event.MouseEvent evt) {                                         
        btnproveedor.setBorder(BorderFactory.createLineBorder(new Color(255,255,255)));
    }                                        

    private void btnventaMouseExited(java.awt.event.MouseEvent evt) {                                     
    btnventa.setBorder(BorderFactory.createLineBorder(new Color(255,255,255)));
    }                                    

    private void btnventaMouseMoved(java.awt.event.MouseEvent evt) {                                    
     btnventa.setBorder(BorderFactory.createLineBorder(new Color(211,84,0)));
    }                                   

    private void btncompraMouseMoved(java.awt.event.MouseEvent evt) {                                     
     btncompra.setBorder(BorderFactory.createLineBorder(new Color(211,84,0)));
    }                                    

    private void btncompraMouseExited(java.awt.event.MouseEvent evt) {                                      
     btncompra.setBorder(BorderFactory.createLineBorder(new Color(255,255,255)));
    }                                     

    private void btnproductoMouseExited(java.awt.event.MouseEvent evt) {                                        
      btnproducto.setBorder(BorderFactory.createLineBorder(new Color(255,255,255)));
    }                                       

    private void btnproductoMouseMoved(java.awt.event.MouseEvent evt) {                                       
      btnproducto.setBorder(BorderFactory.createLineBorder(new Color(211,84,0)));
    }                                      

    private void btnbancoMouseMoved(java.awt.event.MouseEvent evt) {                                    
       btnbanco.setBorder(BorderFactory.createLineBorder(new Color(211,84,0)));
    }                                   

    private void btnusuarioMouseMoved(java.awt.event.MouseEvent evt) {                                      
      btnusuario.setBorder(BorderFactory.createLineBorder(new Color(211,84,0)));
    }                                     

    private void btnusuarioMouseExited(java.awt.event.MouseEvent evt) {                                       
    btnusuario.setBorder(BorderFactory.createLineBorder(new Color(255,255,255)));
    }                                      

    private void btnbancoMouseExited(java.awt.event.MouseEvent evt) {                                     
      btnbanco.setBorder(BorderFactory.createLineBorder(new Color(255,255,255)));
    }                                 
     private void btnclienteMouseClicked(java.awt.event.MouseEvent evt) {                                        
        

            // Mostrar formulario
            
            vistaCliente cliente = new vistaCliente();
            cliente.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            cliente.setVisible(true);
    }
     private void btnbancoMouseClicked(java.awt.event.MouseEvent evt) {                                        
        //vistaCliente.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

            // Mostrar formulario
            
            vistaBanco banco = new vistaBanco();
            banco.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            banco.setVisible(true);
    }  
     private void btnusuarioMouseClicked(java.awt.event.MouseEvent evt) {                                        
        //vistaCliente.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

            // Mostrar formulario
            
            vistaUsuario usuario = new vistaUsuario();
            usuario.setVisible(true);
    } 
     private void btnproveedorMouseClicked(java.awt.event.MouseEvent evt) {                                        
        //vistaCliente.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

            // Mostrar formulario
            
            vistaProveedor proveedor = new vistaProveedor();
            proveedor.setVisible(true);
    } 
     
          private void btnproductoMouseClicked(java.awt.event.MouseEvent evt) {                                        
        //vistaCliente.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

            // Mostrar formulario
            
            vistaProducto producto = new vistaProducto();
            producto.setVisible(true);
    } 
           private void btncompraMouseClicked(java.awt.event.MouseEvent evt) {                                        
        //vistaCliente.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

            // Mostrar formulario
            
            vistaCompra compra = new vistaCompra();
            compra.setVisible(true);
    } 
            private void btnventaMouseClicked(java.awt.event.MouseEvent evt) {                                        
        //vistaCliente.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

            // Mostrar formulario
            
            vistaVenta venta = new vistaVenta();
            venta.setVisible(true);
    } 

                       
    private javax.swing.JLabel btnbanco;
    private javax.swing.JLabel btncliente;
    private javax.swing.JLabel btncompra;
    private javax.swing.JLabel btnproducto;
    private javax.swing.JLabel btnproveedor;
    private javax.swing.JLabel btnusuario;
    private javax.swing.JLabel btnventa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel logoempresa;
    private javax.swing.JLabel nombreEmpresa;
    // End of variables declaration                   
}