package vistas;

import java.awt.Color;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import modelo.dao.InventarioController;
import modelo.dao.InventarioControllerImpl;
import modelo.entidades.Inventario;
import modelo.excepciones.InventarioException;
import utilidades.InventarioTableModel;

public class vistaProducto extends javax.swing.JFrame {

    public vistaProducto() {
        initComponents();
        this.setLocationRelativeTo(null);
        PanelAgregar.setVisible(false);
        PanelEditar.setVisible(false);
        PanelConsultar.setVisible(false);
        PanelEliminar.setVisible(false);
    }

    @SuppressWarnings("unchecked")
                          
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        PanelEliminar = new javax.swing.JPanel();
        cmbProveedor = new javax.swing.JComboBox<>();
        btnborrar = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        nombreEmpresa3 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        PanelAgregar = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtproveedor = new javax.swing.JTextField();
        txtnif = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtmail = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txttelefono = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        btnguardar = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        nombreEmpresa1 = new javax.swing.JLabel();
        PanelEditar = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtnombreedit = new javax.swing.JTextField();
        txtnifedit = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtdireccionedit = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtmailedit = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txttlfedit = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        btnmodificar = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        nombreEmpresa2 = new javax.swing.JLabel();
        cmbeditprov = new javax.swing.JComboBox<>();
        btnconf = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        PanelConsultar = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProveedor = new javax.swing.JTable();
        logoempresa = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnconsultar = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        nombreEmpresa = new javax.swing.JLabel();
        btnagregar = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btneditar = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btneliminar = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        nombreEmpresa4 = new javax.swing.JLabel();

        setTitle("EASY START - PRODUCTOS");
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 125, 99));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Open Sans", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 102));
        jLabel3.setText("PRODUCTO");
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo1_1.jpg"))); // NOI18N
        jPanel4.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, -30, -1, -1));

        PanelEliminar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        
        PanelEliminar.add(cmbProveedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 150, 350, 50));

        btnborrar.setBackground(new java.awt.Color(0, 204, 204));
        btnborrar.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btnborrar.setForeground(new java.awt.Color(0, 51, 51));
        btnborrar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnborrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-lápiz-24 (1).png"))); // NOI18N
        btnborrar.setText("ELIMINAR");
        btnborrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnborrar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnborrar.setIconTextGap(20);
        
        btnborrar.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseClicked(java.awt.event.MouseEvent evt) {
        btnborrarMouseClicked(evt);
        }});
        
       
        PanelEliminar.add(btnborrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 310, 170, 50));

        jPanel10.setBackground(new java.awt.Color(250, 175, 4));
        jPanel10.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        nombreEmpresa3.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        nombreEmpresa3.setForeground(new java.awt.Color(255, 255, 255));
        nombreEmpresa3.setText("ELIMINAR");
        jPanel10.add(nombreEmpresa3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 250, 40));

        PanelEliminar.add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 310, 180, 50));

        jLabel15.setBackground(new java.awt.Color(0, 125, 99));
        jLabel15.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 51, 51));
        jLabel15.setText("PRODUCTO");
        PanelEliminar.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, -1, -1));

        jPanel4.add(PanelEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, 550, 430));

        PanelAgregar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setBackground(new java.awt.Color(0, 125, 99));
        jLabel4.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 51, 51));
        jLabel4.setText("NOMBRE");
        PanelAgregar.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 60, -1, -1));

       
        PanelAgregar.add(txtproveedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 50, 340, 40));

       
        PanelAgregar.add(txtnif, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 110, 340, 40));

        jLabel5.setBackground(new java.awt.Color(0, 125, 99));
        jLabel5.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 51, 51));
        jLabel5.setText("STOCK");
        PanelAgregar.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 120, -1, -1));

        
        PanelAgregar.add(txtdireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 170, 340, 40));

        jLabel6.setBackground(new java.awt.Color(0, 125, 99));
        jLabel6.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 51, 51));
        jLabel6.setText("PRECIO");
        PanelAgregar.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 180, -1, -1));

       
        PanelAgregar.add(txtmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 230, 340, 40));

        jLabel7.setBackground(new java.awt.Color(0, 125, 99));
        jLabel7.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 51, 51));
        jLabel7.setText("DESCRIPCIÓN");
        PanelAgregar.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, -1, -1));

       
        PanelAgregar.add(txttelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 290, 340, 40));

        jLabel8.setBackground(new java.awt.Color(0, 125, 99));
        jLabel8.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 51, 51));
        jLabel8.setText("IMAGEN");
        PanelAgregar.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 300, -1, -1));

        btnguardar.setBackground(new java.awt.Color(0, 204, 204));
        btnguardar.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btnguardar.setForeground(new java.awt.Color(0, 51, 51));
        btnguardar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-más-32.png"))); // NOI18N
        btnguardar.setText("GUARDAR");
        btnguardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnguardar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnguardar.setIconTextGap(20);
        
        btnguardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardarMouseClicked(evt);
            }
           
        });
       
        PanelAgregar.add(btnguardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 350, 170, 50));

        jPanel8.setBackground(new java.awt.Color(250, 175, 4));
        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        nombreEmpresa1.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        nombreEmpresa1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel8.add(nombreEmpresa1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 250, 40));

        PanelAgregar.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 350, 180, 50));

        jPanel4.add(PanelAgregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, 550, 430));

        PanelEditar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel9.setBackground(new java.awt.Color(0, 125, 99));
        jLabel9.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 51, 51));
        jLabel9.setText("NOMBRE");
        PanelEditar.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, -1, -1));

        
        PanelEditar.add(txtnombreedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 70, 340, 40));

        
        PanelEditar.add(txtnifedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 130, 340, 40));

        jLabel10.setBackground(new java.awt.Color(0, 125, 99));
        jLabel10.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 51, 51));
        jLabel10.setText("STOCK");
        PanelEditar.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, -1, -1));

       
        PanelEditar.add(txtdireccionedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 190, 340, 40));

        jLabel11.setBackground(new java.awt.Color(0, 125, 99));
        jLabel11.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 51, 51));
        jLabel11.setText("PRECIO");
        PanelEditar.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 200, -1, -1));

       
        PanelEditar.add(txtmailedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 250, 340, 40));

        jLabel12.setBackground(new java.awt.Color(0, 125, 99));
        jLabel12.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 51, 51));
        jLabel12.setText("DESCRIPCIÓN");
        PanelEditar.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, -1, -1));

       
        PanelEditar.add(txttlfedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 310, 340, 40));

        jLabel13.setBackground(new java.awt.Color(0, 125, 99));
        jLabel13.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 51, 51));
        jLabel13.setText("IMAGEN");
        PanelEditar.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, -1, -1));

        btnmodificar.setBackground(new java.awt.Color(0, 204, 204));
        btnmodificar.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btnmodificar.setForeground(new java.awt.Color(0, 51, 51));
        btnmodificar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnmodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-lápiz-24 (1).png"))); // NOI18N
        btnmodificar.setText("EDITAR");
        btnmodificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnmodificar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnmodificar.setIconTextGap(20);
        
       btnmodificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnmodificarMouseClicked(evt);
            }
        });
      
        PanelEditar.add(btnmodificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 370, 140, 50));

        jPanel9.setBackground(new java.awt.Color(250, 175, 4));
        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        nombreEmpresa2.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        nombreEmpresa2.setForeground(new java.awt.Color(255, 255, 255));
        jPanel9.add(nombreEmpresa2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 250, 40));

        PanelEditar.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 370, 160, 50));

        cmbeditprov.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        PanelEditar.add(cmbeditprov, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 10, 340, 40));

        jLabel14.setBackground(new java.awt.Color(0, 125, 99));
        jLabel14.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 51, 51));
        jLabel14.setText("NOMBRE");
        PanelEditar.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, -1, -1));
        
        btnconf.setBackground(new java.awt.Color(0, 204, 204));
        btnconf.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btnconf.setForeground(new java.awt.Color(0, 51, 51));
        btnconf.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnconf.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-lápiz-24 (1).png"))); // NOI18N
        btnconf.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnconf.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnconf.setIconTextGap(20);
        
        btnconf.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnconfMouseClicked(evt);
            }
        });
        
        PanelEditar.add(btnconf, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 10, 30, 30));
        
        jPanel11.setBackground(new java.awt.Color(250, 175, 4));
        jPanel11.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        
        nombreEmpresa4.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        nombreEmpresa4.setForeground(new java.awt.Color(255, 255, 255));
        jPanel11.add(nombreEmpresa4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 250, 40));

        PanelEditar.add(jPanel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 10, 30, 30));

        jPanel4.add(PanelEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, 550, 440));

        PanelConsultar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

       
        jScrollPane1.setViewportView(tblProveedor);

        PanelConsultar.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, -1, 240));
        tblProveedor.setFont(new java.awt.Font("Open Sans", 0, 14));

        jPanel4.add(PanelConsultar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, 550, 430));

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 0, 936, 590));
        jPanel1.add(logoempresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 24, 240, 100));

        jLabel2.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Empresa:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, -1, -1));

        btnconsultar.setBackground(new java.awt.Color(255, 255, 255));
        btnconsultar.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btnconsultar.setForeground(new java.awt.Color(0, 51, 51));
        btnconsultar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnconsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-archivo-de-verificación-32 (1).png"))); // NOI18N
        btnconsultar.setText("CONSULTAR");
        btnconsultar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnconsultar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnconsultar.setIconTextGap(20);
        btnconsultar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btnconsultarMouseMoved(evt);
            }
        });
        btnconsultar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnconsultarMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnconsultarMouseExited(evt);
            }
        });
        jPanel1.add(btnconsultar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 410, 290, 50));

        jPanel2.setBackground(new java.awt.Color(250, 175, 4));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        nombreEmpresa.setFont(new java.awt.Font("Open Sans", 0, 18)); // NOI18N
        nombreEmpresa.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(nombreEmpresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 250, 40));

        btnagregar.setBackground(new java.awt.Color(0, 204, 204));
        btnagregar.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btnagregar.setForeground(new java.awt.Color(0, 51, 51));
        btnagregar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-más-32.png"))); // NOI18N
        btnagregar.setText("AGREGAR");
        btnagregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnagregar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnagregar.setIconTextGap(20);
        btnagregar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btnagregarMouseMoved(evt);
            }
        });
        btnagregar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnagregarMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnagregarMouseExited(evt);
            }
        });
        jPanel2.add(btnagregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 290, 50));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 290, 50));

        jPanel3.setBackground(new java.awt.Color(250, 175, 4));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 410, 290, 50));

        jPanel5.setBackground(new java.awt.Color(250, 175, 4));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btneditar.setBackground(new java.awt.Color(0, 153, 153));
        btneditar.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btneditar.setForeground(new java.awt.Color(0, 51, 51));
        btneditar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btneditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-lápiz-24 (1).png"))); // NOI18N
        btneditar.setText("EDITAR");
        btneditar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btneditar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btneditar.setIconTextGap(20);
        btneditar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btneditarMouseMoved(evt);
            }
        });
        btneditar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btneditarMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btneditarMouseExited(evt);
            }
        });
        jPanel5.add(btneditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 290, 50));

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 290, 290, 50));

        jPanel6.setBackground(new java.awt.Color(250, 175, 4));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btneliminar.setBackground(new java.awt.Color(255, 255, 255));
        btneliminar.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        btneliminar.setForeground(new java.awt.Color(0, 51, 51));
        btneliminar.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btneliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-cerrar-ventana-32 (1).png"))); // NOI18N
        btneliminar.setText("ELIMINAR");
        btneliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btneliminar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btneliminar.setIconTextGap(20);
        btneliminar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btneliminarMouseMoved(evt);
            }
        });
        btneliminar.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseClicked(java.awt.event.MouseEvent evt) {
                btneliminarMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btneliminarMouseExited(evt);
            }
        });
        jPanel6.add(btneliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 290, 50));

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 290, 50));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1093, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 579, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                        

    private void btnagregarMouseMoved(java.awt.event.MouseEvent evt) {                                      
      btnagregar.setBorder(BorderFactory.createLineBorder(new Color(153,153,153)));
    }                                     

    private void btneditarMouseMoved(java.awt.event.MouseEvent evt) {                                     
     btneditar.setBorder(BorderFactory.createLineBorder(new Color(153,153,153)));
    }                                    

    private void btneliminarMouseMoved(java.awt.event.MouseEvent evt) {                                       
       btneliminar.setBorder(BorderFactory.createLineBorder(new Color(153,153,153)));
    }                                      

    private void btnconsultarMouseMoved(java.awt.event.MouseEvent evt) {                                        
        btnconsultar.setBorder(BorderFactory.createLineBorder(new Color(153,153,153)));
    }                                       

    private void btnagregarMouseExited(java.awt.event.MouseEvent evt) {                                       
     btnagregar.setBorder(BorderFactory.createLineBorder(new Color(0,125,99)));
    }                                      

    private void btneditarMouseExited(java.awt.event.MouseEvent evt) {                                      
    btneditar.setBorder(BorderFactory.createLineBorder(new Color(0,125,99)));
    }                                     

    private void btneliminarMouseExited(java.awt.event.MouseEvent evt) {                                        
      btneliminar.setBorder(BorderFactory.createLineBorder(new Color(0,125,99)));
    }                                       

    private void btnconsultarMouseExited(java.awt.event.MouseEvent evt) {                                         
      btnconsultar.setBorder(BorderFactory.createLineBorder(new Color(0,125,99)));
    }                         
    
    private void btnagregarMouseClicked(java.awt.event.MouseEvent evt) {                                        
        PanelAgregar.setVisible(true);
        PanelEditar.setVisible(false);
        PanelConsultar.setVisible(false);
        PanelEliminar.setVisible(false);
    }  
    private void btneditarMouseClicked(java.awt.event.MouseEvent evt) {                                        
        PanelAgregar.setVisible(false);
        PanelEditar.setVisible(true);
        PanelConsultar.setVisible(false);
        PanelEliminar.setVisible(false);
        txtnombreedit.setVisible(false);
        txtnifedit.setVisible(false);
        txtdireccionedit.setVisible(false);
        txtmailedit.setVisible(false);
        txttlfedit.setVisible(false);
        btnmodificar.setVisible(false);
        jLabel10.setVisible(false);
        jLabel11.setVisible(false);
        jLabel12.setVisible(false);
        jLabel13.setVisible(false);
        jLabel14.setVisible(false);
        jLabel9.setVisible(false);
       try {
       
        InventarioController i = new InventarioControllerImpl();
        
        List<Inventario> inventario = i.list();
        
        //DefaultComboBoxModel<Proveedor> modelo = new DefaultComboBoxModel<>();

        for (Inventario inv : inventario) {
            
            cmbeditprov.addItem(inv); 
        }
        
        
        } catch (InventarioException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error Aplicación",
                        JOptionPane.ERROR_MESSAGE);
        }
    } 
    
    private void btneliminarMouseClicked(java.awt.event.MouseEvent evt) {                                        
        PanelAgregar.setVisible(false);
        PanelEditar.setVisible(false);
        PanelConsultar.setVisible(false);
        PanelEliminar.setVisible(true);
        
        try {
        
        InventarioController i = new InventarioControllerImpl();
        
        List<Inventario> inventario = i.list();

        for (Inventario inv : inventario) {
            cmbProveedor.addItem(inv);
        }
        } catch (InventarioException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error Aplicación",
                        JOptionPane.ERROR_MESSAGE);
        }
        
    } 
    private void btnconsultarMouseClicked(java.awt.event.MouseEvent evt) {                                        
        try {
            PanelAgregar.setVisible(false);
            PanelEditar.setVisible(false);
            PanelConsultar.setVisible(true);
            PanelEliminar.setVisible(false);
            
            InventarioController inven = new InventarioControllerImpl();
            List<Inventario> consultar = inven.list();
            
            tblProveedor.setModel(new InventarioTableModel(consultar));
            tblProveedor.getTableHeader().setReorderingAllowed(false);
            tblProveedor.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        } catch (InventarioException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error Aplicación",
                        JOptionPane.ERROR_MESSAGE);
        }
    
    } 
    
    private void btnguardarMouseClicked(java.awt.event.MouseEvent evt) { 
        
        try {
        String producto = txtproveedor.getText();
        String stock = txtnif.getText();
        String precio_vent = txtdireccion.getText();
        String descripcion = txtmail.getText();
        String imagen= txttelefono.getText();
        
        Inventario  i = new Inventario(producto,Integer.parseInt(stock),Double.parseDouble(precio_vent),descripcion,imagen);
        InventarioController inven = new InventarioControllerImpl();
        inven.crear(i);
        } catch (InventarioException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error Aplicación",
                        JOptionPane.ERROR_MESSAGE);
            }
        JOptionPane.showMessageDialog(null, "Se ha creado un nuevo Producto");
    
    } 
    
    
    private void btnborrarMouseClicked(java.awt.event.MouseEvent evt) { 
        try {
        Integer idinventario = cmbeditprov.getItemAt(cmbeditprov.getSelectedIndex()).getId_inventario();
        
        
       // Proveedor  p = new Proveedor(idproveedor);
        InventarioController inven = new InventarioControllerImpl();
        Inventario eliminarInv = inven.eliminar(idinventario);
        } catch (InventarioException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error Aplicación",
                        JOptionPane.ERROR_MESSAGE);
            }
        JOptionPane.showMessageDialog(null, "Se ha eliminado el proveedor");
    
    } 
    
    private void btnconfMouseClicked(java.awt.event.MouseEvent evt) {                                     

        txtnombreedit.setText(cmbeditprov.getItemAt(cmbeditprov.getSelectedIndex()).getNombre());
        txtnifedit.setText(cmbeditprov.getItemAt(cmbeditprov.getSelectedIndex()).getStock().toString());
        txtdireccionedit.setText(cmbeditprov.getItemAt(cmbeditprov.getSelectedIndex()).getPrecio_vent().toString());
        txtmailedit.setText(cmbeditprov.getItemAt(cmbeditprov.getSelectedIndex()).getDescripcion());
        txttlfedit.setText(cmbeditprov.getItemAt(cmbeditprov.getSelectedIndex()).getImagen());
        
        txtnombreedit.setVisible(true);
        txtnifedit.setVisible(true);
        txtdireccionedit.setVisible(true);
        txtmailedit.setVisible(true);
        txttlfedit.setVisible(true);
        btnmodificar.setVisible(true);
        jLabel10.setVisible(true);
        jLabel11.setVisible(true);
        jLabel12.setVisible(true);
        jLabel13.setVisible(true);
        jLabel14.setVisible(true);
        jLabel9.setVisible(true);
        
    }
     private void btnmodificarMouseClicked(java.awt.event.MouseEvent evt) {                                          
        try {
            int codigo = cmbeditprov.getItemAt(cmbeditprov.getSelectedIndex()).getId_inventario();
            InventarioController inven = new InventarioControllerImpl();
            Inventario modificarInven = inven.findById(codigo);
            modificarInven.setNombre(txtnombreedit.getText());
            modificarInven.setStock(Integer.parseInt(txtnifedit.getText()));
            modificarInven.setPrecio_vent(Double.parseDouble(txtdireccionedit.getText()));
            modificarInven.setDescripcion(txtmailedit.getText());
            modificarInven.setImagen(txttlfedit.getText());
            inven.editar(modificarInven);
        } catch (InventarioException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error Aplicación",
                        JOptionPane.ERROR_MESSAGE);
            }
         JOptionPane.showMessageDialog(null, "Se ha editado el producto correctamente");
        
    } 
                      
    private javax.swing.JPanel PanelAgregar;
    private javax.swing.JPanel PanelConsultar;
    private javax.swing.JPanel PanelEditar;
    private javax.swing.JPanel PanelEliminar;
    private javax.swing.JLabel btnagregar;
    private javax.swing.JLabel btnconsultar;
    private javax.swing.JLabel btnconf;
    private javax.swing.JLabel btneditar;
    private javax.swing.JLabel btneliminar;
    private javax.swing.JLabel btnguardar;
    private javax.swing.JLabel btnmodificar;
    private javax.swing.JLabel btnborrar;
    private javax.swing.JComboBox<Inventario> cmbeditprov;
    private javax.swing.JComboBox<Inventario> cmbProveedor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblProveedor;
    private javax.swing.JTextField txtproveedor;
    private javax.swing.JTextField txttlfedit;
    private javax.swing.JTextField txtnif;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtmail;
    private javax.swing.JTextField txttelefono;
    private javax.swing.JTextField txtnombreedit;
    private javax.swing.JTextField txtnifedit;
    private javax.swing.JTextField txtdireccionedit;
    private javax.swing.JTextField txtmailedit;
    private javax.swing.JLabel logoempresa;
    private javax.swing.JLabel nombreEmpresa;
    private javax.swing.JLabel nombreEmpresa1;
    private javax.swing.JLabel nombreEmpresa2;
    private javax.swing.JLabel nombreEmpresa3;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JLabel nombreEmpresa4;
    
               
}
