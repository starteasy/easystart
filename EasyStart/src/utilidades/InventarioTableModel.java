package utilidades;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import modelo.entidades.Inventario;

public class InventarioTableModel extends AbstractTableModel{
    
    private List<Inventario> inventario = new ArrayList<>();

    public InventarioTableModel(List<Inventario> i) {
        this.inventario = i;
    }

    @Override
    public int getRowCount() {
        return inventario.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int fila, int columna) {
        switch (columna) {
            case 0:
                return inventario.get(fila).getNombre();
            case 1:
                return inventario.get(fila).getStock();
            case 2:
                return inventario.get(fila).getPrecio_vent();
            case 3:
                return inventario.get(fila).getDescripcion();
            case 4:
                return inventario.get(fila).getImagen();
            
            default:
                return null;
        }
    }
    @Override
    public String getColumnName(int columna) {
        switch (columna) {
            case 0:
                return "NOMBRE";
            case 1:
                return "STOCK";
            case 2:
                return "PRECIO";
            case 3:
                return "DESCRIPCIÓN";
            case 4:
                return "IMAGEN";
            default:
                return null;
        }
    }
    
}
