package utilidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public interface BaseDatos {

    
    
        String URL = "jdbc:mysql://localhost:3306/easy_start";
	String USER = "root";
	String PASSWORD = "Pa$$w0rd";

	//String SELECT = "SELECT COUNT(*) FROM reservas.reservas WHERE id_cliente = ?";
	String INSERT_PROVEEDOR = "INSERT INTO easy_start.proveedores(nombre,nif,direccion,email,telefono) VALUES(?,?,?,?,?)";
        String EDIT_PROVEEDOR = "UPDATE easy_start.proveedores SET nombre = ?,nif=?,direccion=?,email=?,telefono=?  WHERE id_proveedor= ?";
        String INSERT_COMPRAR = "INSERT INTO easy_start.compras(id_proveedor,fecha) VALUES(?,CURDATE())";
        String INSERT_COMPRARDET = "INSERT INTO easy_start.compras_det(id_compra,id_inventario,cantidad,precio,total) VALUES(?,?,?,?,?)";
        String INSERT_INVENTARIO = "INSERT INTO easy_start.inventario(nombre,stock,precio_venta,descripcion,imagen) VALUES(?,?,?,?,?)";
       String  EDIT_INVENTARIO = "UPDATE easy_start.inventario SET nombre = ?,stock=?,precio_venta=?,descripcion=?,imagen=?  WHERE id_inventario= ?";
       
       
        String INSERT_USUARIOS = "INSERT INTO easy_start.usuarios (usuario, password, privilegios) values (?,?,?)";
        String UPDATE_USUARIOS = "UPDATE easy_start.usuarios set usuario = ?, password = ?, privilegios = ? where id_usuario = ?";
        
        
        String SELECT_BANCOS = "SELECT bancos.id_banco, bancos.entidad, bancos.monto_inicial, bancos.monto_actual   FROM easy_start.bancos";
        String INSERT_BANCOS = "INSERT INTO easy_start.bancos (entidad, monto_inicial, monto_actual) VALUES(?,?,?)";
        String UPDATE_BANCOS = "UPDATE easy_start.bancos set entidad = ?, monto_inicial = ?, monto_actual = ? WHERE id_banco = ?";
        String DELETE_BANCOS = "DELETE FROM easy_start.bancos WHERE id_banco = ?";
        
       
        
}