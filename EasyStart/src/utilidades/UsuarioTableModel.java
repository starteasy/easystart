package utilidades;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import modelo.entidades.Usuario;

public class UsuarioTableModel extends AbstractTableModel {

    private List<Usuario> usuario = new ArrayList<>();

    public UsuarioTableModel(List<Usuario> u) {
        this.usuario = u;
    }

    @Override
    public int getRowCount() {
        return usuario.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int fila, int columna) {
        switch (columna) {
            case 0:
                return usuario.get(fila).getUsuario();
            case 1:
                return usuario.get(fila).getContraseña();
            case 2:
                return usuario.get(fila).getPrivilegios();
                
                default:
                    return null;
        }
    }

    @Override
    public String getColumnName(int columna) {
        switch (columna) {
            case 0:
                return "USUARIO" ;
            case 1:
                return "CONTRASEÑA" ;
            case 2:
                return "PRIVILEGIOS";
                
                default:
                    return null;
    }
    }

}
