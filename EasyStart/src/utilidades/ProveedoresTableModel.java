package utilidades;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import modelo.entidades.Proveedor;

public class ProveedoresTableModel extends AbstractTableModel{
    
    private List<Proveedor> proveedor = new ArrayList<>();

    public ProveedoresTableModel(List<Proveedor> p) {
        this.proveedor = p;
    }

    @Override
    public int getRowCount() {
        return proveedor.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int fila, int columna) {
        switch (columna) {
            case 0:
                return proveedor.get(fila).getProveedor();
            case 1:
                return proveedor.get(fila).getNif();
            case 2:
                return proveedor.get(fila).getDireccion();
            case 3:
                return proveedor.get(fila).getEmail();
            case 4:
                return proveedor.get(fila).getTelefono();
            
            default:
                return null;
        }
    }
    @Override
    public String getColumnName(int columna) {
        switch (columna) {
            case 0:
                return "NOMBRE";
            case 1:
                return "NIF";
            case 2:
                return "DIRECCIÓN";
            case 3:
                return "EMAIL";
            case 4:
                return "TELÉFONO";
            default:
                return null;
        }
    }
    
}
