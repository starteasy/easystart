package utilidades;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/*
    Generar el contenido celda de un JTable mediante una etiqueta
 */
public class LabelRenderer implements TableCellRenderer {

    private JLabel lblCelda = new JLabel();

    private Color defaultBackground = lblCelda.getBackground();

    /*
        Método para devolver un componente que se va a utilizar para
        generar el contenido de una celda de la tabla
     */
    @Override
    public Component getTableCellRendererComponent(JTable table,
            Object valor, boolean isSelected, boolean hasFocus, int fila, int columna) {

        // Asignar valor a la etiqueta
        lblCelda.setText(valor.toString());

        // Poner la misma fuente que tiene (familia y tamaño) y poner contenido
        // en negrita y cursiva
        lblCelda.setFont(new Font(lblCelda.getFont().getName(),
                Font.BOLD | Font.ITALIC, lblCelda.getFont().getSize()));

        // Comprobar si estaba seleccionada
        lblCelda.setBackground(isSelected ? table.getSelectionBackground()
                : defaultBackground);

        return lblCelda;

    }

}
