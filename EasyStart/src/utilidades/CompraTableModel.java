package utilidades;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import modelo.entidades.CompraDet;
import modelo.entidades.Inventario;

public class CompraTableModel extends AbstractTableModel {

    private List<CompraDet> compraDet;

    public CompraTableModel() {
        compraDet = new ArrayList<>();
    }
    
    
    
    @Override
    public int getRowCount() {
        return compraDet.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int fila, int columna) {
        /*if(compraDet.size() == fila){
            return null;
        }*/
        //if(compraDet.size() > 0){
       switch (columna) {
            case 0:
                return compraDet.get(fila).getId_inventario();
            case 1:
                return compraDet.get(fila).getCantidad() != null ?compraDet.get(fila).getCantidad():0;
            case 2:
                return compraDet.get(fila).getPrecio()!= null ?compraDet.get(fila).getPrecio():0;
            case 3:
                return compraDet.get(fila).getTotal()!= null ?compraDet.get(fila).getTotal():0;
            default:
                return null;
        }
        //}
         //return null;
    }
    
    public void addRow(CompraDet c) {

        int index = compraDet.size();
        compraDet.add(c);

        fireTableRowsInserted(index, index);
    }
    
    public void deleteRow(int index) {
        // Comprobar que es indice valida
        if (index >= 0 && index < compraDet.size()) {
            compraDet.remove(index);

            // Notificar que se ha eliminado una fila
            fireTableRowsDeleted(index, index);
        }
    }
    
    @Override
    public String getColumnName(int columna) {
        switch (columna) {
            case 0:
                return "PRODUCTO";
            case 1:
                return "CANTIDAD";
            case 2:
                return "PRECIO";
            case 3:
                return "TOTAL";
            default:
                return null;
        }
     }
    public Class<?> getColumnClass(int columna) {
        switch (columna) {
            case 0:
                return CompraDet.class;
            case 1:
                return Integer.class;
            case 2:
                return Double.class;
            case 3:
                return Double.class;
            default:
                return null;
        }
    }
    @Override
    public boolean isCellEditable(int fila, int columna) {
         switch (columna) {
            case 0:
                return true;
            case 1:
                return true;
            case 2:
                return true;
            case 3:
                return false;
            default:
                return true;
        }
    }
    @Override
    public void setValueAt(Object valor, int fila, int columna) {
       
        //if (valor instanceof String) {
        /*if(compraDet.size() == 0){
            compraDet.add(new CompraDet());
        }
        
         if(compraDet.size() == fila){
            return;
        }*/
            switch (columna) {
                case 0:
                    compraDet.get(fila).setId_inventario(((Inventario)valor).getId_inventario());
                    break;
                case 1:
                    compraDet.get(fila).setCantidad(Integer.parseInt(valor.toString()));
                    break;
                case 2:
                    compraDet.get(fila).setPrecio(Double.parseDouble(valor.toString()));
                    break;
                case 3:
                    compraDet.get(fila).setTotal(Double.parseDouble(valor.toString()));
                    break;
   
            }
            
            // Notoficacion de modificacion del valor de una celda
            fireTableCellUpdated(fila, columna);
        }
        //}
    
}
