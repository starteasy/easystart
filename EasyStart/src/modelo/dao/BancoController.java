
package modelo.dao;

import java.util.List;
import modelo.entidades.Banco;
import modelo.excepciones.BancoException;


public interface BancoController {
    
    List<Banco> list() throws BancoException;
    
    Banco findById(int idBanco) throws BancoException;
    
    Banco crear(Banco banco) throws BancoException;
    
    void modificar(Banco banco) throws BancoException;
    
    Banco eliminar(int idBanco) throws BancoException;
    
    
}
