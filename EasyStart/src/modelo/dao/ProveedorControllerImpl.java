package modelo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.entidades.Proveedor;
import modelo.excepciones.ProveedorException;
import utilidades.BaseDatos;

public class ProveedorControllerImpl implements ProveedorController{
    
    private Connection getConnection() throws SQLException{
    return DriverManager.getConnection(BaseDatos.URL,BaseDatos.USER,BaseDatos.PASSWORD);
    }

    @Override
    public List<Proveedor> list() throws ProveedorException {
        try {
	
	Connection connection = getConnection();

	Statement st = connection.createStatement();
        
	ResultSet rs = st.executeQuery("SELECT id_proveedor,nombre,nif,direccion,email,telefono FROM easy_start.proveedores");

	List<Proveedor> listaproveedores = new ArrayList<>();


        if (rs.next()) {

	do {
	int id_proveedor = rs.getInt(1);
        String nombre = rs.getString(2);     
        String nif = rs.getString(3);
        String direccion = rs.getString(4);
        String email = rs.getString(5);
        Integer telefono = rs.getInt(6);

	Proveedor p = new Proveedor(id_proveedor, nombre,nif,direccion,email,telefono);

        listaproveedores.add(p);

	} while (rs.next());

	}

	connection.close();

	return listaproveedores;

		} catch (SQLException e) {

		throw new ProveedorException(e.getMessage(), e);
		}
    }

    @Override
    public Proveedor findById(int idproveedor) throws ProveedorException {
        try {
	Connection connection = getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery("SELECT nombre,nif,direccion,email,telefono FROM easy_start.proveedores " + "WHERE id_proveedor=" + idproveedor);
        Proveedor p = null;
	if (rs.next()) {
        String proveedor = rs.getString(1);
        String nif = rs.getString(2);
        String direccion = rs.getString(3);
        String email = rs.getString(4);
        Integer telefono = rs.getInt(5);
        p = new Proveedor(idproveedor,proveedor,nif,direccion,email,telefono);

	}
        connection.close();
        return p;
        } catch (SQLException e) {
	throw new ProveedorException(e.getMessage(), e);
	}
    }

    @Override
    public Proveedor crear(Proveedor proveedor) throws ProveedorException {
        try {
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(BaseDatos.INSERT_PROVEEDOR);
            ps.setString(1, proveedor.getProveedor());
            ps.setString(2, proveedor.getNif());
            ps.setString(3, proveedor.getDireccion());
            ps.setString(4, proveedor.getEmail());
            ps.setInt(5, proveedor.getTelefono());
            
            ps.executeUpdate();
            return(proveedor);
        } catch (SQLException ex) {
            throw  new ProveedorException(ex.getMessage(), ex);
        }
        
    }

    @Override
    public void editar(Proveedor proveedor) throws ProveedorException {
        try {

	Connection connection = getConnection();
        PreparedStatement ps = connection.prepareStatement(BaseDatos.EDIT_PROVEEDOR);

	ps.setString(1, proveedor.getProveedor());
        ps.setString(2, proveedor.getNif());
        ps.setString(3, proveedor.getDireccion());
        ps.setString(4, proveedor.getEmail());
        ps.setInt(5, proveedor.getTelefono());
        ps.setInt(6, proveedor.getIdproveedor());
        
        ps.executeUpdate();
        
        } catch (SQLException ex) {
            throw  new ProveedorException(ex.getMessage(), ex);
        }
    }

    @Override
    public Proveedor eliminar(int idproveedor) throws ProveedorException {
        try {
	
	Connection connection = getConnection();

	Statement st = connection.createStatement();

	Proveedor p = findById(idproveedor);

	st.executeUpdate("DELETE FROM easy_start.proveedores WHERE id_proveedor=" + idproveedor);

	connection.close();

	return p;

	} catch (SQLException e) {
	throw new ProveedorException(e.getMessage(), e);
	}
    }
    
}
