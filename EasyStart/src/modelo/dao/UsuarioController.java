package modelo.dao;

import java.util.Collection;
import java.util.List;
import modelo.entidades.Usuario;
import modelo.excepciones.UsuarioException;


public interface UsuarioController {
    
    List<Usuario> listaUsuarios() throws UsuarioException;
    
    Usuario findbyId(int idUsuario) throws UsuarioException;
    
    Usuario crear(Usuario usuario) throws UsuarioException;
    
    void modificar(Usuario usuario) throws UsuarioException;
    
    Usuario eliminar(int idUsuario) throws UsuarioException;
    
    
   
    
}
