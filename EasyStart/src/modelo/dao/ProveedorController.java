package modelo.dao;

import java.util.List;
import modelo.entidades.Proveedor;
import modelo.excepciones.ProveedorException;

public interface ProveedorController {

	List<Proveedor> list() throws ProveedorException;

	Proveedor findById(int idproveedor) throws ProveedorException;
	
	Proveedor crear(Proveedor proveedor) throws ProveedorException;

	void editar(Proveedor proveedor) throws ProveedorException;

	Proveedor eliminar(int idproveedor) throws ProveedorException;
}
