package modelo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.entidades.CompraDet;
import modelo.excepciones.CompraDetExeption;
import utilidades.BaseDatos;

public class CompraDetControllerImpl implements CompraDetController{
    
    private Connection getConnection() throws SQLException{
    return DriverManager.getConnection(BaseDatos.URL,BaseDatos.USER,BaseDatos.PASSWORD);
    }

    @Override
    public CompraDet crear(CompraDet compradet) throws CompraDetExeption {
          try {
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(BaseDatos.INSERT_COMPRARDET);
            ps.setInt(1, compradet.getId_compra());
            ps.setInt(2, compradet.getId_inventario());
            ps.setInt(1, compradet.getCantidad());
            ps.setDouble(1, compradet.getPrecio());
            ps.setDouble(1, compradet.getTotal());
            ps.executeUpdate();
            return compradet;
        } catch (SQLException ex) {
            throw  new CompraDetExeption(ex.getMessage(), ex);
        }
    }

    @Override
    public List<CompraDet> list() throws CompraDetExeption {
        try {
	
	Connection connection = getConnection();

	Statement st = connection.createStatement();
        
	ResultSet rs = st.executeQuery("SELECT id_proveedor,nombre,nif,direccion,email,telefono FROM easy_start.proveedores");

	List<CompraDet> listacompradet = new ArrayList<>();


        if (rs.next()) {

	do {
	Integer id_compra_det = rs.getInt(1);
        Integer id_compra = rs.getInt(2);    
        Integer id_inventario = rs.getInt(3);
        Integer cantidad = rs.getInt(4);
        Double precio = rs.getDouble(5);
        Double total = rs.getDouble(6);

	CompraDet c = new CompraDet(id_compra_det, id_compra,id_inventario,cantidad,precio,total);

        listacompradet.add(c);

	} while (rs.next());

	}

	connection.close();

	return listacompradet;

		} catch (SQLException e) {

		throw new CompraDetExeption(e.getMessage(), e);
		}
    }
    
}
