package modelo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.entidades.Compra;
import modelo.excepciones.CompraException;
import utilidades.BaseDatos;

public class CompraControllerImpl implements CompraController{
    
    private Connection getConnection() throws SQLException{
    return DriverManager.getConnection(BaseDatos.URL,BaseDatos.USER,BaseDatos.PASSWORD);
    }

    @Override
    public Compra crear(Compra compra) throws CompraException {
        try {
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(BaseDatos.INSERT_COMPRAR);
            ps.setInt(1, compra.getId_proveedor());
            ps.executeUpdate();
            
            ResultSet rs = ps.executeQuery("SELECT id_compra FROM " + 
			"easy_start.compras WHERE id_compra ='" + compra.getId_compra() + "'");

		rs.next();

		int id_compra = rs.getInt("id_compra");

		connection.close();

		return new Compra(id_compra);
        } catch (SQLException ex) {
            throw  new CompraException(ex.getMessage(), ex);
        }
    }
    
}
