package modelo.dao;

import java.util.List;
import modelo.entidades.Inventario;
import modelo.excepciones.InventarioException;

public interface InventarioController {
    
        List<Inventario> list() throws InventarioException;

	Inventario findById(int idinventario) throws InventarioException;
	
	Inventario crear(Inventario inventario) throws InventarioException;

	void editar(Inventario inventario) throws InventarioException;

	Inventario eliminar(int idinventario) throws InventarioException;
    
}
