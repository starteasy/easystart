package modelo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.entidades.Usuario;
import modelo.excepciones.UsuarioException;
import utilidades.BaseDatos;


public class UsuarioControllerImpl implements UsuarioController{
   
    private Connection getConnection() throws SQLException{
        return DriverManager.getConnection(BaseDatos.URL, BaseDatos.USER, BaseDatos.PASSWORD);
    }

    @Override
    public List<Usuario> listaUsuarios() throws UsuarioException {
        
        try {
                 Connection connection = getConnection();
                
                Statement st = connection.createStatement();
                
                ResultSet rs = st.executeQuery("SELECT id_usuario, usuario, password, privilegios FROM easy_start.usuarios");
                
                List<Usuario> listaUsuarios = new ArrayList<>();
                
                if(rs.next()){
                   
                    
                    do{
                        int idusuario = rs.getInt(1);
                        String usuario = rs.getString(2);
                        String password = rs.getString(3);
                        int privilegios = rs.getInt(4);
                        
                        Usuario us = new Usuario(idusuario, usuario, usuario, privilegios);
                        
                        listaUsuarios.add(us);
                        
                    }while (rs.next());
                }
                connection.close();
                
                return listaUsuarios;
                
            } catch(SQLException ex){
                throw new UsuarioException(ex.getMessage(), ex);
            
            }
    }
       
    
    @Override
    public Usuario findbyId(int idUsuario) throws UsuarioException {
        
        try{
           Connection connection = getConnection();
            
            Statement st = connection.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT usuario, password, privilegios FROM easy_start.usuarios"  + "WHERE   id_usuario=" + idUsuario);
            
            Usuario u = null;
            
            if(rs.next()){
                String usuario = rs.getString(1);
                String contraseña = rs.getString(2);
                Integer privilegios = rs.getInt(3);
                
                u = new Usuario(idUsuario, usuario, contraseña, privilegios);
            }
            connection.close();
            return u;
        } catch (SQLException ex){
            throw new UsuarioException(ex.getMessage(), ex);
        }
        
    
}

    @Override
    public Usuario crear(Usuario usuario) throws UsuarioException {
        
        
        
        try {
            Connection connection = getConnection();
            
            PreparedStatement ps = connection.prepareStatement(BaseDatos.INSERT_USUARIOS);
            
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getContraseña());
            ps.setInt(3, usuario.getPrivilegios());
            
            ps.executeUpdate();
            return (usuario);
        } catch (SQLException ex) {
            throw new UsuarioException(ex.getMessage(), ex);
        } 
    }

    @Override
    public void modificar(Usuario usuario) throws UsuarioException {
        
        
         try {
            Connection connection = getConnection();
            
            PreparedStatement ps = connection.prepareStatement(BaseDatos.UPDATE_USUARIOS);
            
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getContraseña());
            ps.setInt(3, usuario.getPrivilegios());
            
            ps.executeUpdate();
            
        } catch(SQLException ex) {
            throw new UsuarioException(ex.getMessage(), ex);
        } 
    }

    @Override
    public Usuario eliminar(int idUsuario) throws UsuarioException {
        
        
        try{
            Connection connection = getConnection();
            
            Statement st = connection.createStatement();
            
            Usuario u = findbyId(idUsuario);
            
            st.executeUpdate("DELETE FROM easy_start.usuarios WHERE id_usuario = " + idUsuario);
            
            connection.close();
            
            return u;
        } catch (SQLException ex){
            throw new UsuarioException(ex.getMessage(), ex);
        } 
    }

    
}
