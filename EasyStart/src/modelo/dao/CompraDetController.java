package modelo.dao;

import java.util.List;
import modelo.entidades.CompraDet;
import modelo.excepciones.CompraDetExeption;

public interface CompraDetController {
     List<CompraDet> list() throws CompraDetExeption;
     CompraDet crear(CompraDet compradet) throws CompraDetExeption;
}
