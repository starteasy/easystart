package modelo.dao;

import modelo.entidades.Compra;
import modelo.excepciones.CompraException;

public interface CompraController {
    Compra crear(Compra compra) throws CompraException;
}
