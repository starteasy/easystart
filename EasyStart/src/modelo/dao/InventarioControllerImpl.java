package modelo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.entidades.Inventario;
import modelo.excepciones.InventarioException;
import utilidades.BaseDatos;

public class InventarioControllerImpl implements InventarioController{
    
    private Connection getConnection() throws SQLException{
    return DriverManager.getConnection(BaseDatos.URL,BaseDatos.USER,BaseDatos.PASSWORD);
    }
    
    @Override
    public List<Inventario> list() throws InventarioException {
        try {
	
	Connection connection = getConnection();

	Statement st = connection.createStatement();
        
	ResultSet rs = st.executeQuery("SELECT id_inventario,nombre,stock,precio_venta,descripcion,imagen FROM easy_start.inventario");

	List<Inventario> listainventario = new ArrayList<>();


        if (rs.next()) {

	do {
	int id_inventario = rs.getInt(1);
        String nombre = rs.getString(2);     
        Integer stock = rs.getInt(3);
        Double precio_venta = rs.getDouble(4);
        String descripcion = rs.getString(5);
        String imagen = rs.getString(6);

	Inventario i = new Inventario(id_inventario, nombre,stock,precio_venta,descripcion,imagen);

        listainventario.add(i);

	} while (rs.next());

	}

	connection.close();

	return listainventario;

		} catch (SQLException e) {

		throw new InventarioException(e.getMessage(), e);
		}
    }

    @Override
    public Inventario findById(int idinventario) throws InventarioException {
        try {
	Connection connection = getConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery("SELECT nombre,stock,precio_venta,descripcion,imagen FROM easy_start.inventario " + "WHERE id_inventario=" + idinventario);
        Inventario i = null;
	if (rs.next()) {
        String nombre = rs.getString(1);     
        Integer stock = rs.getInt(2);
        Double precio_venta = rs.getDouble(3);
        String descripcion = rs.getString(4);
        String imagen = rs.getString(5);
        i = new Inventario(idinventario,nombre,stock,precio_venta,descripcion,imagen);

	}
        connection.close();
        return i;
        } catch (SQLException e) {
	throw new InventarioException(e.getMessage(), e);
	}
    }

    @Override
    public Inventario crear(Inventario inventario) throws InventarioException {
        try {
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(BaseDatos.INSERT_INVENTARIO);
            ps.setString(1, inventario.getNombre());
            ps.setInt(2, inventario.getStock());
            ps.setDouble(3, inventario.getPrecio_vent());
            ps.setString(4, inventario.getDescripcion());
            ps.setString(5, inventario.getImagen());
            
            ps.executeUpdate();
            return(inventario);
        } catch (SQLException ex) {
            throw  new InventarioException(ex.getMessage(), ex);
        }
    }

    @Override
    public void editar(Inventario inventario) throws InventarioException {
        try {

	Connection connection = getConnection();
        PreparedStatement ps = connection.prepareStatement(BaseDatos.EDIT_INVENTARIO);

	ps.setString(1, inventario.getNombre());
        ps.setInt(2, inventario.getStock());
        ps.setDouble(3, inventario.getPrecio_vent());
        ps.setString(4, inventario.getDescripcion());
        ps.setString(5, inventario.getImagen());
        ps.setInt(6, inventario.getId_inventario());
        
        ps.executeUpdate();
        
        } catch (SQLException ex) {
            throw  new InventarioException(ex.getMessage(), ex);
        }
    }

    @Override
    public Inventario eliminar(int idinventario) throws InventarioException {
        try {
	
	Connection connection = getConnection();

	Statement st = connection.createStatement();

	Inventario i = findById(idinventario);

	st.executeUpdate("DELETE FROM easy_start.inventario WHERE id_inventario=" + idinventario);

	connection.close();

	return i;

	} catch (SQLException e) {
	throw new InventarioException(e.getMessage(), e);
	}
    }
    
}
