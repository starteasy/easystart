package modelo.entidades;


public class Banco {
    
    private Integer idBanco;
    private String entidad;
    private Double montoInicial;
    private Double montoActual;

    public Banco(Integer idBanco) {
        this.idBanco = idBanco;
    }

    public Banco(Integer idBanco, String entidad, Double montoInicial, Double montoActual) {
        this.idBanco = idBanco;
        this.entidad = entidad;
        this.montoInicial = montoInicial;
        this.montoActual = montoActual;
    }

    public Banco(String entidad, Double montoInicial, Double montoActual) {
        this.entidad = entidad;
        this.montoInicial = montoInicial;
        this.montoActual = montoActual;
    }

    public Integer getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Integer idBanco) {
        this.idBanco = idBanco;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Double getMontoInicial() {
        return montoInicial;
    }

    public void setMontoInicial(Double montoInicial) {
        this.montoInicial = montoInicial;
    }

    public Double getMontoActual() {
        return montoActual;
    }

    public void setMontoActual(Double montoActual) {
        this.montoActual = montoActual;
    }

    @Override
    public String toString() {
        return "Banco{" + "idBanco=" + idBanco + ", entidad=" + entidad + ", montoInicial=" + montoInicial + ", montoActual=" + montoActual + '}';
    }

    

   
    
    
    
    
}
