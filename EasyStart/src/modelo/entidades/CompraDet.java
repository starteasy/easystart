package modelo.entidades;

public class CompraDet {
    private Integer id_compra_det;
    private Integer id_compra;
    private Integer id_inventario;
    private Integer cantidad;
    private Double precio;
    private Double total;

    public CompraDet() {
    }

    public CompraDet(Integer id_compra_det) {
        this.id_compra_det = id_compra_det;
    }

    public CompraDet(Integer id_inventario,Integer cantidad, Double precio, Double total) {
        this.id_inventario = id_inventario;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
    }
    
    public CompraDet(Integer id_compra, Integer id_inventario, Integer cantidad, Double precio, Double total) {
        this.id_compra = id_compra;
        this.id_inventario = id_inventario;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
    }

    public CompraDet(Integer id_compra_det, Integer id_compra, Integer id_inventario, Integer cantidad, Double precio, Double total) {
        this.id_compra_det = id_compra_det;
        this.id_compra = id_compra;
        this.id_inventario = id_inventario;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
    }

    public Integer getId_compra_det() {
        return id_compra_det;
    }

    public Integer getId_compra() {
        return id_compra;
    }

    public Integer getId_inventario() {
        return id_inventario;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public Double getPrecio() {
        return precio;
    }

    public Double getTotal() {
        return total;
    }

    public void setId_compra(Integer id_compra) {
        this.id_compra = id_compra;
    }

    public void setId_inventario(Integer id_inventario) {
        this.id_inventario = id_inventario;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
    

    @Override
    public String toString() {
        return "CompraDet{" + "id_compra_det=" + id_compra_det + ", id_compra=" + id_compra + ", id_inventario=" + id_inventario + ", cantidad=" + cantidad + ", precio=" + precio + ", total=" + total + '}';
    }
    
    
}
