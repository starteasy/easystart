package modelo.entidades;

public class Inventario {
    private Integer id_inventario;
    private String nombre;
    private Integer stock;
    private Double precio_vent;
    private String descripcion;
    private String imagen;
    
    public Inventario() {
    }

    public Inventario(Integer id_inventario, String nombre, Integer stock, Double precio_vent, String descripcion, String imagen) {
        this.id_inventario = id_inventario;
        this.nombre = nombre;
        this.stock = stock;
        this.precio_vent = precio_vent;
        this.descripcion = descripcion;
        this.imagen = imagen;
    }

    public Inventario(String nombre, Integer stock, Double precio_vent, String descripcion, String imagen) {
        this.nombre = nombre;
        this.stock = stock;
        this.precio_vent = precio_vent;
        this.descripcion = descripcion;
        this.imagen = imagen;
    }

    public Integer getId_inventario() {
        return id_inventario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Double getPrecio_vent() {
        return precio_vent;
    }

    public void setPrecio_vent(Double precio_vent) {
        this.precio_vent = precio_vent;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
    
}
