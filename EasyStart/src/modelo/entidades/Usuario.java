package modelo.entidades;


public class Usuario {
    
    private Integer idUsuario;
    private String usuario;
    private String contraseña;
    private Integer privilegios;

    public Usuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    

    public Usuario(Integer idUsuario, String usuario, String contraseña, Integer privilegios) {
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.privilegios = privilegios;
    }

    public Usuario(String usuario, String contraseña, Integer privilegios) {
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.privilegios = privilegios;
    }
    
    

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public Integer getPrivilegios() {
        return privilegios;
    }

    public void setPrivilegios(Integer privilegios) {
        this.privilegios = privilegios;
    }

    @Override
    public String toString() {
        return usuario + ", contrase\u00f1a=" + contraseña + ", privilegios=" + privilegios + '}';
    }
    

    
   

    
    
    
}
