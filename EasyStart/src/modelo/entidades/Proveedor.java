package modelo.entidades;

public class Proveedor {
    private Integer idproveedor;
    private String proveedor;
    private String nif;
    private String direccion;
    private String email;
    private Integer telefono;

    public Proveedor(Integer idproveedor) {
        this.idproveedor = idproveedor;
    }

    public Proveedor(Integer idproveedor, String proveedor, String nif, String direccion, String email, Integer telefono) {
        this.idproveedor = idproveedor;
        this.proveedor = proveedor;
        this.nif = nif;
        this.direccion = direccion;
        this.email = email;
        this.telefono = telefono;
    }
   

    public Proveedor(String proveedor, String nif, String direccion, String email, Integer telefono) {
        this.idproveedor = idproveedor;
        this.proveedor = proveedor;
        this.nif = nif;
        this.direccion = direccion;
        this.email = email;
        this.telefono = telefono;
    }
    
    public Proveedor() {
    }

    public Integer getIdproveedor() {
        return idproveedor;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return proveedor;
    }
    
    
    
}
