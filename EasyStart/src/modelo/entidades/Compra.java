package modelo.entidades;

public class Compra {
    private Integer id_compra;
    private Integer id_proveedor;

    public Compra() {
    }

    public Compra(Integer id_proveedor) {
    this.id_proveedor = id_proveedor;
    }

    public Integer getId_compra() {
        return id_compra;
    }

    public Integer getId_proveedor() {
        return id_proveedor;
    }

    @Override
    public String toString() {
        return "Compra{" + "id_proveedor=" + id_proveedor + '}';
    }

}
