package modelo.excepciones;

public class CompraDetExeption extends Exception{
    	public CompraDetExeption(String mensaje) {
		super(mensaje);
	}

	public CompraDetExeption(String mensaje, Throwable error) {
		super(mensaje, error);
	}
    
}
