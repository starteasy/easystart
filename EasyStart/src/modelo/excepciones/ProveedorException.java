package modelo.excepciones;

public class ProveedorException extends Exception{
    	public ProveedorException(String mensaje) {
		super(mensaje);
	}

	public ProveedorException(String mensaje, Throwable error) {
		super(mensaje, error);
	}
}
