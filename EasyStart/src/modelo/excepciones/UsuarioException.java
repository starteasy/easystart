
package modelo.excepciones;


public class UsuarioException extends Exception{
 
    public UsuarioException(String mensaje){
        super(mensaje);
    }
    
    public UsuarioException(String mensaje, Throwable error){
        super(mensaje, error);
    }
}
