package modelo.excepciones;

public class CompraException extends Exception{
    	public CompraException(String mensaje) {
		super(mensaje);
	}

	public CompraException(String mensaje, Throwable error) {
		super(mensaje, error);
	}
    
}
