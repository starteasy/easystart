
package modelo.excepciones;

public class InventarioException extends Exception{
    	public InventarioException(String mensaje) {
		super(mensaje);
	}

	public InventarioException(String mensaje, Throwable error) {
		super(mensaje, error);
	}
    
}
